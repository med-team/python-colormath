Source: python-colormath
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Dylan Aïssi <daissi@debian.org>
Section: python
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-setuptools,
               python3-networkx,
               python3-numpy
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-colormath
Vcs-Git: https://salsa.debian.org/python-team/packages/python-colormath.git
Homepage: https://github.com/gtaylor/python-colormath
Rules-Requires-Root: no

Package: python3-colormath
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Description: Abstracts common color math operations (Python 3 version)
 python-colormath is a simple Python module that spares the user from directly
 dealing with color math. Some features include: Support for a wide range of
 color spaces. A good chunk of the CIE spaces, RGB, HSL/HSV, CMY/CMYK, and many
 more. Conversions between the various color spaces. For example, XYZ to sRGB,
 Spectral to XYZ, CIE Lab to Adobe RGB. Calculation of color difference. All
 CIE Delta E functions, plus CMC. Chromatic adaptations (changing illuminants).
 RGB to hex and vice-versa. 16-bit RGB support.
 .
 This package contains the Python 3 version of the library.
